package com.project.project;

import com.project.project.api.controller.CustomerController;
import com.project.project.api.model.Customer;
import com.project.project.service.impl.CustomerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringJUnitConfig
public class CustomerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private CustomerController customerController;

    @Mock
    private CustomerServiceImpl customerService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
    }

    @Test
    void testCreateCustomer() throws Exception {
        // Create a  customer object to send in the request
        Customer customer = new Customer();
        customer.setRegistrationCode("12345");
        customer.setFullName("John Doe");
        customer.setEmail("john.doe@example.com");
        customer.setTelephone("123-456-7890");

        //doNothing().when(customerService).createCustomer(customer);

        // Perform the HTTP POST request to create a customer
        mockMvc.perform(MockMvcRequestBuilders.post("/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"registrationCode\":\"12345\",\"fullName\":\"John Doe\",\"email\":\"john.doe@example.com\",\"telephone\":\"123-456-7890\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
