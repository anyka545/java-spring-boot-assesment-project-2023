package com.project.project;

import com.project.project.api.controller.ProductController;
import com.project.project.api.model.Product;
import com.project.project.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.doNothing;

public class ProductTest {

    private MockMvc mockMvc;

    @InjectMocks
    private ProductController productController;

    @Mock
    private ProductServiceImpl productService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
    }

    @Test
    void testCreateProduct() throws Exception {
        // Create a sample product object to send in the request
        Product product = new Product();
        product.setName("Sample Product");
        product.setSkuCode("SKU123");
        product.setUnitPrice(19.99);

        // Mock the behavior of the productService when createProduct is called
        doNothing().when(productService).createProduct(product);

        // Perform the HTTP POST request to create a product
        mockMvc.perform(MockMvcRequestBuilders.post("/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Sample Product\",\"skuCode\":\"SKU123\",\"unitPrice\":19.99}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
