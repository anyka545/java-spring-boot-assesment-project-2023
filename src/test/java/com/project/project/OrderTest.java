package com.project.project;
import com.project.project.api.model.Order;
import com.project.project.repostories.OrderRepo;

import com.project.project.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class OrderTest {

    @InjectMocks
    OrderServiceImpl orderService;

    @Mock
    OrderRepo orderRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        orderService = new OrderServiceImpl(orderRepository);
    }

    @Test
    void createOrder() {}

    @Test
    void searchAllOrdersByDate() {
        Order order = new Order();
        String date = "2023-09-12";
        order.setDate(Date.valueOf(date));
        List<Order> orderList = new ArrayList<>();
        orderList.add(order);

        when(orderRepository.findAll()).thenReturn(orderList);

        List<Order> orders = orderService.searchAllOrdersByDate(date);
        assertEquals(1,orders.size() );
    }
}