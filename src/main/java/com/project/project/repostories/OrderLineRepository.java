package com.project.project.repostories;

import com.project.project.api.model.OrderLine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("orderLineRepository")
public interface OrderLineRepository extends JpaRepository<OrderLine, Long> {


}
