package com.project.project.service;

import com.project.project.api.model.Order;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public interface OrderService {
    void createOrder(Order order);

    List<Order> searchAllOrdersByDate(String date);
}