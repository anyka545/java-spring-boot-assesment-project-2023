package com.project.project.service;

import com.project.project.api.model.Product;
import com.project.project.repostories.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    void createProduct (Product product);
}


