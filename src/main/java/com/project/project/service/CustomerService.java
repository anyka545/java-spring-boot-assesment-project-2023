package com.project.project.service;

import com.project.project.api.model.Customer;
import com.project.project.repostories.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface CustomerService {
    void createCustomer(Customer customer);

    List<Customer> getAllCustomers();
}
