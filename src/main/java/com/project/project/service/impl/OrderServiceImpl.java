package com.project.project.service.impl;

import com.project.project.api.model.Order;
import com.project.project.repostories.OrderRepo;
import com.project.project.service.OrderService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepo orderRepository;

    public OrderServiceImpl(@Qualifier("orderRepository") OrderRepo orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public void createOrder(Order order) {
        orderRepository.save(order);
    }

    @Override
    public List<Order> searchAllOrdersByDate(String date) {
        return orderRepository.findAll().stream().filter(e -> e.getDate().equals(Date.valueOf(date))).collect(Collectors.toList());
    }
}
