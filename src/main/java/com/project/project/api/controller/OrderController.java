

package com.project.project.api.controller;

import com.project.project.api.model.Customer;
import com.project.project.api.model.Order;
import com.project.project.api.model.OrderLine;
import com.project.project.api.model.Product;
import com.project.project.service.CustomerService;
import com.project.project.service.OrderService;
import com.project.project.service.ProductService;
import com.project.project.service.impl.OrderServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
@RestController
@RequestMapping("/order")
public class OrderController {

    OrderServiceImpl orderService;

    public OrderController(OrderServiceImpl orderService) {
        this.orderService = orderService;
    }

    @RequestMapping(value = "",method = RequestMethod.POST)
    public void createOrder(@RequestBody Order order){
        orderService.createOrder(order);
    }

    @RequestMapping(value = "/{date}", method = RequestMethod.GET)
    public List<Order> getOrdersByDate(@PathVariable String date){
        return orderService.searchAllOrdersByDate(date);
    }
}